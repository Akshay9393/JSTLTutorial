<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
     
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>c:choose,c:when,c:otherwise example</title>
</head>
<body>
  
<h2>c:choose,c:when,c:otherwise example</h2>
 
 
<c:choose>
    <%-- When color parameter == 'red' --%>
    <c:when test="${param.color=='red'}">
        <p style="color:red;">RED COLOR</p>
    </c:when>  
     
    <%-- When color parameter == 'blue' --%>
    <c:when test="${param.color=='blue'}">
        <p style="color:blue;">BLUE COLOR</p>
    </c:when>  
      
    <%-- Otherwise --%> 
    <c:otherwise>
        <b>Other color</b>
    </c:otherwise>
</c:choose>
 
</body>
</html>